\documentclass[10pt,a4paper]{article}
\usepackage[latin1]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{fancyref}
\usepackage{listings}
\author{Toon Lenaerts s4321219}
\begin{document}
\section{Results}
In this section, we present our results for the three improvements we made to Verleg's work. In Section \ref{results model} we show the state machine we inferred from the OpenSSH implementation. Then, in Section \ref{results testing} we give some statistics about the used test algorithm, and explain some properties we found in the generated model. Finally, in Section \ref{results modelchecking} we show the LTL properties we checked with the model.

\subsection{Trained Model} \label{results model}
The result of mapping OpenSSH is a model with 26 states and 619 transitions. An edited version can be seen in Figure \ref{resultmachine}. The happy path, as defined by Verleg, is indicated in green.  A more complete version, also edited for readability, has been included in the appendices. The more complete version contains nine states that can be reached by attempting to open a channel before performing user authentication. These states make for a less readable model, however, they do not contain any usable behavior of the SSH protocol; Although they can perform a successful authentication, no channels can be opened, and all possible paths from these states eventually lead to a disconnect-state. Therefore we have removed them from Figure \ref{resultmachine}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.2]{100perc.png} 
\caption{Inferred state machine, edited for readability}
\label{resultmachine}
\end{figure}



\subsection{Testing} \label{results testing}
We used the Yannakakis-tool on the generated hypotheses. For the final hypothesis, we completed an exhaustive test up to a path length of two. During the entire mapping process, only two hypotheses were made, the counter example to the first hypothesis was found after 865 tests. Verifying the second hypothesis took 32728 tests.

When we compare our model with Verleg's model, we can find some significant differences. To illustrate them, we isolated the connection layer from Figure \ref{resultmachine}, and emphasized the differences. The result is shown in Figure \ref{modeldifference}. Verleg's state machine of the connection layer of OpenSSH showed a fault, where closing a channel from the ``has\_commands\_pty'' would result in connection termination. Our model does not show this behavior, closing a channel from the ``has\_commands\_pty'' state leads to a new state, from which other actions are still allowed. We have checked these differences with our setup, and have found that our model holds. This means that Verleg's model contains an error which our model does not have. 

\begin{figure}[h]
\centering
\includegraphics[scale=0.25]{differences.png} 
\caption{Connection layer of the inferred state machine. Transitions in red are unique to Verleg's models, the state and transitions in green are unique to our work.}
\label{modeldifference}
\end{figure}



\subsection{Model Checking} \label{results modelchecking}
We have converted both security properties into LTL specifications and used the NuSMV model checker to check the properties for truthfulness with the state machine we have inferred. Since there are two properties, we expand on each property individually, first the transport layer, then the user authentication layer.

\subsubsection{Transport Layer}
The security property for the Transport layer is defined as follows: ``We consider a transport layer state machine secure if there is no path from the initial state to the point where the authentication service is invoked without exchanging and employing cryptographic keys.'' We consider this path to be a path where a key exchange is performed successfully and followed by a successful service authentication request. Note that the key exchange does not have to be performed in consecutive steps, it can temporarily be interrupted by other actions. 

In natural languange, we can say the property should be something like: ``If the authentication service is invoked successfully, then the final step of the key exchange must have been done, the second step must have been done earlier, and the first step even earlier.'' The key exchange consists of three specific steps which have to be performed in order. The LTL as in Figure \ref{transportltlnatural} is the result. The O-operator used here is the Once-operator, a past time LTL operator, which is true if its argument is true in a past time instant. 

\begin{figure}[h]
\begin{align*}
&G (\text{authentication request successful} \rightarrow \\
&O (\text{NEWKEYS successful } \& \\
&O( \text{KEX\_30 successful } \& \\
&O(\text{ KEXINIT successful}) )))
\end{align*}
\caption{LTL for Transport layer with natural language}
\label{transportltlnatural}
\end{figure}

Translating this LTL to NuSMV syntax means that the remaining natural language has to be replaced with equality checks for the variables in the NuSMV state. These variables are equal to the in- and output of a transition in the Mealy machine generated by the mapper. A successful invocation of the authentication service is translated as a request for the authentication service being done, and the request being accepted by the server. The third step of the key exchange, the client sending a newkeys-packet, is successful if the server sends no response. Conveniently, no server response is an indication that the newkeys was received correctly, in our tests, the server would return a packet if the newkeys was performed incorrectly. The second step, sending the kex30 packet, is very much like the third step. Finally, the first step of the protocol is translated a bit differently, since we found that sending any packet to the server from the initial state was recognized as the first step of the key exchange protocol. Therefore, sending the first step, a kexinit-packet, can be replaced by almost any packet. The resulting LTL-specification is shown in Figure \ref{transportltl}.

\begin{figure}[h]
\begin{lstlisting}[basicstyle=\footnotesize]
G (
    (input=SERVICE_REQUEST_AUTH & output=SERVICE_ACCEPT) 
  -> 
    O ( (input=NEWKEYS & output= NO_RESP) & 
      O ( (input=KEX30 & output=KEX31_NEWKEYS) & 
        O (output=KEXINIT)
    )
  )
)
\end{lstlisting}
\caption{Transport layer LTL in NuSMV syntax.}
\label{transportltl}
\end{figure}

\subsubsection{User Authentication Layer} 
The security property is as follows: ``We consider a user authentication layer state machine secure if there is no path from the unauthenticated state to the authenticated state without providing the correct credentials.'' Since we have no method to directly determine if the model is in an authenticated state, we consider the authenticated state to be a state where opening a channel can be done successfully. Since opening a channel should only be possible after user authentication, this should show if we are in an authenticated state. The specification then is rather simple: Always, if a channel is opened succesfully, then there has not been a failure in user authentication since a successful user authentication. Opening a channel successfully in the model is straightforward, a request to open a channel is sent to the server, and a success message is received back. Checking whether there has been a success or failure in user authentication is done by checking if the server ever sends the UA\_FAILURE or UA\_SUCCESS packets. Additional checks if the user actually made a user authenication request could potentially be added. The resulting LTL is shown in Figure \ref{authenticationltl}. This LTL uses another past time operator: The S- (or Since-) operator is true if its first argument holds for every state since its second argument holds, note that the second argument has to hold somewhere in the path for the since-operator to hold. 

\begin{figure}[h]
\begin{lstlisting}[basicstyle=\footnotesize]
G ( (input= CH_OPEN & output= CH_OPEN_SUCCESS) 
-> (!(output=UA_FAILURE) S output= UA_SUCCESS) )
\end{lstlisting}
\caption{User Authentication layer LTL in NuSMV syntax.}
\label{authenticationltl}
\end{figure}


The learned model satisfies both LTL specifications, according to NuSMV.

\section{Discussion}
In this Section we discuss and motivate some of the choices we made in this thesis. In Section \ref{disc caching} we expand on the caching method we use. Then, in Section \ref{disc confidence} we give our consideration for the testing parameters used. In Section \ref{disc faults} we provide explanation for some faults in the generated model. Finally, in Section \ref{disc property} we expand on the security properties used.

\subsection{Caching} \label{disc caching}
In our setup, we improve upon the caching methods Verleg used. This allows us to resume an old, possibly crashed, run of the setup. Because of this, we are able to build a state machine of the entire SSH protocol. However, it is possible to implement much more extensive methods of caching and detection of non-determinism. For instance, we have not implemented any way of checking a trace which results in non-determinism. This can be done by, for example, sending a trace which results in non-determinism again, and comparing the output of both traces. If the second output does not result in non-determinism, we might assume that something went wrong during the first run of the trace (a packet was lost, for example), and can continue learning with the second output. We decided the caching we used was adequate, and thus have not implemented more advanced methods



\subsection{Chosen Confidence} \label{disc confidence}
For our equivalence oracle, we use the Yannakakis-based algorithm in order to build a measure of confidence for the generated state machine. The confidence increases by running the algorithm with a longer path-length. As explained, we run up to a path length of two. We do this due to time considerations; for our generated model, testing up to a path length of two requires 32.728 tests, A path length of up to four would take 7.276.082 tests. These tests are often long traces of inputs, and running them can take up to a couple of minutes per trace. Testing seven million traces with our setup is not feasible. However, one of Verleg's concerns for training all three layers of SSH simultaneously is the rekey procedure. The concern that the model will not implement the rekey procedure in higher layers correctly. The procedure contains three steps before returning to the state before the rekey. A confidence with a path length up to four should correctly implement the rekey procedure for all the states in the SSH-specification where rekey is possible. To remedy this problem, we have used the same solution Verleg has used. We use a separate command which performs an entire rekey procedure in the setup. Because of this, the model should still handle the entire procedure correctly. However, this does not provide confidence in performing only parts of the rekey procedure, or performing the procedure differently, for example.

\subsection{Faulty rekeys in State Machine} \label{disc faults}
As explained in Section \ref{results testing}, our model does not show the disconnecting behavior Verleg experienced. However, it still contains some strange behavior. In SSH, performing a rekey procedure in the ``higher'' connection layer should not influence actions in that layer. Therefore, in the generated state model, performing a rekey from a certain state in a higher layer should always return to that state upon finishing the procedure. In our model, performing a rekey from the ``authed'', ``has\_channel'' and ``has\_pty'' states ends in the ``authed\_rekeyed'', ``has\_commands'' and ``has\_commands\_pty'' states respectively.  

\subsection{Security property coverage} \label{disc property}
As explained in Section \ref{method checking}, we have not formalized a security property for the connection layer. This however, means that the section of the state machine where some interesting behavior happens (see Section \ref{results testing} and Section \ref{disc faults}) is not covered by a security definition. From observing the problems we have found we have not found any significant security related problem, however, observing by eye for mistakes goes against the goal of this work, and therefore leaves us with a weakness in our models.


\end{document}