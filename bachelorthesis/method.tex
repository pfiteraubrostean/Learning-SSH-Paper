\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{listings}
\author{Toon Lenaerts s4321219}
\title{Method}
\begin{document}

Note: I assume that the reader has read Patrick's thesis, and is therefore familiar with the basic principles of protocol state fuzzing and its terminology. Altough I am not sure if I should explain NuSMV / LTL in more detail.

\section{Method}
Since we change Verlegs work in three ways, we will expand on the three categories of changes separately. In Section 3.1, we explain our changes to Verlegs program to enable mapping of all three SSH-Layers. Then, in Secion 3.2, we provide a brief explanation of the testing algorithm we used. Finally, in Section 3.3, we explain how we used model checking to verify security properties Verleg defined for SSH.

\subsection{Building a complete model}
As said previously, Verleg split building the model of SSH into three layers as defined in the SSH-specification. Part of the reason for this is practical, it takes a very long time to infer the entire model. Combining this with problems of non-determinism Verleg experienced, and lack of handling this non-determinism, meant that inferring the full model would often fail partway through. The other reason Verleg mentioned is that the setup was unable to detect previously run layers interacting when messages are sent, we took this for granted, and merely expected this to cause an increase in state-space.

With the most prominent problems in inferring the entire SSH-protocol being time and the process failing due to non-determinism, we investigated the methods of caching Verleg used in their mapper.  The results of queries were saved during the process, however, it was mainly used to detect non-determinism by comparing the output of partial new queries with the output of old queries. A way to resume an old run of the program through the cache was implemented, but contained errors. We implemented using the cache to resume an earlier run of the program. We did not expand further on handling non-determinism and caching, we explain why in more detail in Section 5: Discussion. In order to reduce the amount of non-determinism, we had to fine-tune time-out parameters in the mapper, this was done mostly on a trial-and-error basis. The result was that running a relatively long query would take a considerable time, and thus that the entire fuzzing-process would take be time-consuming.

Since we want to improve the process of state fuzzing used by Verleg, rather than add upon the size of his research, we only map a single SSH implementation. The implementation used is OpenSSH, version 6.9p1-2, an implementation of SSH that Verleg also used.

\subsection{Improving Testing}
We attempt to improve the testing used in Verlegs process by using a different equivalence oracle. Verleg used a random-walk oracle, this oracle randomly chooses a path through the model, sends the required inputs to the SUT, and checks if the output corresponds to the expected output of the chosen path. This is repeated for some time, until the model is assumed to be correct. We instead use an implementation of an algorithm specified by (?) based on an algorithm by Lee \& Yannakakis to find adaptive distinguishing sequences. 

The algorithm uses distinguishing sequences for every state of the hypothesis, a distinguishing sequence is a set of in- and outputs that provides a unique result for a certain state. Practically, it is possible to check if an unknown state $x$ is actually state $a$ by entering the distinguishing state starting from $x$, if the resulting output is equal to the predicted output, then we started from state $a$. The algorithm, when run exhausively on a model, will check all paths of an incrementing length between states. If the algorithm does not find a counterexample up to a specific path-length, we can say that the tested model is correct, unless the actual model is larger than the checked hypothesis by an amount of states larger than the tested path-length. Typically, the algorithm can be run either exhaustively or randomly, running it randomly is usually faster for finding counter-examples, but does not provide the confidence that using exhaustive testing does.

\subsection{Model Checking}
We use the NuSMV model checker to verify the properties defined by Patrick. NuSMV is a model checker where a state of a model is specified as a set of finite variables, and a transition-function can then be defined to change these variables. Specifications in temporal logic, such as CTL and LTL, can be checked for truth on specified models. NuSMV will provide a counterexample if a certain specification is not true.

The NuSMV model is generated from the result of the fuzzing process, these results are in .dot-format. Note that, as a side-effect of using a simple way to generate the NuSMV-code, we do not use arrays or any form of collection for transitions with multiple packets. Rather, we create a separate word with the concatenation of packets. This can be accounted for in an LTL-specification by simply replacing a packet with the conjunction of all words that contain that packet.

The properties we will verify are LTL-representations of the security properties Verleg defined for each layer of SSH. Verleg has defined three properties. The property of the connection layer, however, is very open ended. It is: ``We will ... look at unexpected state machine transitions that can point towards potential implementation flaws'', we can not properly represent this in LTL. The properties for the transport- and user authentication layers, however, can be represented in LTL.


\subsubsection{Transport Layer}
The security property for the Transport layer is defined as follows: ``We consider a transport layer state machine secure if there is no path from the initial state to the point where the authentication service is invoked without exchanging and employing cryptographic keys.'' We consider this path to be a path where a key exchange is performed successfully and followed by a successful service authentication request. Note that the key exchange does not have to be performed in consecutive steps, it can be temporarily be interrupted by other actions. 

In natural languange, we can say the property should be something like: ``If the authentication service is invoked successfully, then the final step of the key exchange must have been done, the second step must have been done earlier, and the first step even earlier.'' The key exchange consists of three specific steps which have to be performed in order. The LTL as in figure 1 is the result. The O-operator used here is the Once-operator, a past time LTL operator, which is true if its argument is true in a past time instant. 

\begin{figure}[h]
\begin{align*}
&G (\text{authentication request successful} \rightarrow \\
&O (\text{NEWKEYS successful } \& \\
&O( \text{KEX\_30 successful } \& \\
&O(\text{ KEXINIT successful}) )))
\end{align*}
\caption{LTL for Transport layer with natural language}
\end{figure}

Translating this LTL to NuSMV syntax means that the remaining natural language has to be replaced with equality checks for the variables in the NuSMV state. These variables are equal to the in- and output of a transition in the Mealy machine generated by the mapper. A successful invocation of the authentication service is translated as a request for the authentication service being done, and the request being accepted by the server. The third step of the key exchange, the client sending a newkeys-packet, is successful if the server sends no response. Conveniently, no server response is an indication that the newkeys was received correctly, in our tests, the server would return a packet if the newkeys was performed incorrectly. The second step, sending the kex30 packet, is very much like the third step. Finally, the first step of the protocol is translated a bit differently, since we found that sending any packet to the server from the initial state was recognized as the first step of the key exchange protocol. Therefore, sending the first step, a kexinit-packet, can be replaced by almost any packet.

\begin{figure}[h]
\begin{lstlisting}[basicstyle=\footnotesize]
G (
    (input=SERVICE_REQUEST_AUTH & output=SERVICE_ACCEPT) 
  -> 
    O ( (input=NEWKEYS & output= NO_RESP) & 
      O ( (input=KEX30 & output=KEX31_NEWKEYS) & 
        O (output=KEXINIT)
    )
  )
)
\end{lstlisting}
\caption{Transport layer LTL in NuSMV syntax.}
\end{figure}

\subsubsection{User Authentication Layer} 
The security property is as follows: ``We consider a user authentication layer state machine secure if there is no path from the unauthenticated state to the authenticated state without providing the correct credentials.'' Since we have no method to directly determine if the model is in an authenticated state, we consider the authenticated state to be a state where opening a channel can be done successfully. Since opening a channel should only be possible after user authentication, this should show if we are in an authenticated state. The specification then is rather simple: Always, if a channel is opened succesfully, then there has not been a failure in user authentication since a successful user authentication. Opening a channel successfully in the model is straightforward, a request to open a channel is sent to the server, and a success message is received back. Checking whether there has been a success or failure in user authentication is done by checking if the server ever sends the UA\_FAILURE or UA\_SUCCESS packets. Additional checks if the user actually made a user authenication request could potentially be added. The resulting LTL is shown in figure 3. This LTL uses another past time operator: The S- (or Since-) operator is true if its first argument holds for every state since its second argument holds, note that the second argument has to hold somewhere in the path for the since-operator to hold. 

\begin{figure}[h]
\begin{lstlisting}[basicstyle=\footnotesize]
G ( (input= CH_OPEN & output= CH_OPEN_SUCCESS) 
-> (!(output=UA_FAILURE) S output= UA_SUCCESS) )
\end{lstlisting}
\caption{User Authentication layer LTL in NuSMV syntax.}
\end{figure}

\section{Results}


\subsection{Trained Model}
The result of mapping OpenSSH is a model with 26 states and 619 transitions. A version, edited for readability has been included in the appendices. (Note: I'm still working on this readable version, apologies.)

\subsection{Testing}
We used the Yannakakis-tool on the generated hypotheses. For the final hypothesis, we completed an exhaustive test up to a path length of two. During the entire mapping process, only two hypotheses were made, the counter example to the first hypothesis was found after 865 tests. Verifying the second hypothesis took 32728 tests.

\subsection{Model Checking}
Both LTL specifications are true for the generated model, according to NuSMV.

\end{document}