\section{Security specifications} \label{sec:specs}

%\newfloat{property}{thp}{lop}
%\floatname{property}{Property}

  \newtheorem{property}{Property}

%The size of the models makes them difficult to manually inspect and verify against specifications. Manual analysis is further %complicated by the ambiguity present in textual specifications.
%Hence it makes sense to (1) formalize specification so as to eliminate ambiguity, and (2) use model checking to verify the %specifications automatically.  To these ends, we use the NuSMV model checker \cite{NuSMV2} to verify security properties for the %learned models, properties which we formalize using LTL formulas.  

A NuSMV model is specified by a set of finite variables together with a transition-function that describes changes on these variables. Specifications in temporal logic, such as CTL and LTL, can be checked for truth on specified models. NuSMV provides a counterexample if a given specification is not true. We generate NuSMV models automatically from the learned models. Generation proceeds by first defining a NuSMV file with three variables, corresponding to inputs, outputs and states. The transition-function is then extracted from the learned model and appended to this file. 
This function updates the output and state variables for a given valuation of the input variable and the current state.  Figure~\ref{fig:nusmvex} gives an example of a Mealy machine and its associated NuSMV model.

%\parbox{\columnwidth}{
%  \parbox{0.5\columnwidth}{
%\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=2.8cm]
%  \node[initial,state] (q0)      {$q_0$};
%  \node[state]         (q1) [right of=q0]  {$q_1$};
%
%  \path[->]          (q0)  	edge  						   node {INIT/OK} (q1);
%	\path[->]          (q0)  	edge  [loop above]   node {MSG/NOK} (q0);
%	\path[->]          (q1)  	edge  [loop above]   node {INIT/OK} (q1);
%  \path[->]          (q1)  	edge  [loop right] 	 node {MSG/ACK} (q1);
%\end{tikzpicture}	
%}
%\parbox{0.5\columnwidth}{
%\begin{verbatim}
%
%\end{verbatim}
%}
%}

%\lstset{showspaces=true}


\begin{figure}[htbp]
\begin{tabular}{p{0.17\textwidth}p{0.17\textwidth}}
    \begin{minipage}{.17\textwidth}
    \centering
		\begin{tikzpicture}[scale=0.1,>=stealth',shorten >=1pt,auto,node distance=2.5cm]
		\footnotesize
			\node[initial,state] (q0)      {$q_0$};
			\node[state]         (q1) [below of=q0]  {$q_1$};

			\path[->]          (q0)  	edge  						   node {BEGIN/OK} (q1);
			\path[->]          (q0)  	edge  [loop above]   node {MSG/NOK} (q0);
			\path[->]          (q1)  	edge  [loop right]   node {BEGIN/OK} (q1);
			\path[->]          (q1)  	edge  [loop below] 	 node {MSG/ACK} (q1);
		\end{tikzpicture}
    \end{minipage}
    &
    \begin{minipage}{.17\textwidth}
\tiny
\begin{lstlisting}
	MODULE main 
		VAR state : {q0, q1};
		inp : {BEGIN, MSG};
		out : {OK, NOK, ACK};
		ASSIGN
		init(state) := q0;
		next(state) := case
			state = q0 & inp = BEGIN: q1;
			state = q0 & inp = MSG: q0; 
			state = q1 & inp = BEGIN: q1;
			state = q1 & inp = MSG: q1; 
	    esac;
		out := case
			state = q0 & inp = BEGIN: OK;
			state = q0 & inp = MSG: NOK;
			state = q1 & inp = BEGIN: OK;
			state = q1 & inp = MSG: ACK;
		esac;
\end{lstlisting}
\end{minipage}
\end{tabular}
		\caption{Mealy machine + associated NuSMV code}
		\label{fig:nusmvex}
\end{figure}

The remainder of this section defines the properties we formalized and verified. We group these properties into four categories: 

\begin{enumerate}
\item \textit{basic characterizing properties}, properties which characterize the {\dmapper} and {\dsut} assembly at a basic level. These hold for all implementations.
\item \textit{security properties}, these are properties fundamental to achieving the main security goal of the respective layer.
\item \textit{key re-exchange properties}, or properties regarding the rekey operation (after the initial key exchange was done).
\item \textit{functional properties}, which are extracted from the SHOULD's and the MUST's of the RFC specifications. They may have a security impact.
\end{enumerate}

A key point to note is that properties are checked not on the actual concrete model of the {\dsut}, but on an abstraction of the {\dsut} that is induced by the {\dmapper}. This is unlike in~\cite{TCP2016}, where properties where checked on a concretization of the learned model obtained by application of a reverse mapping. Building a reverse mapper is far from trivial given the {\dmapper}'s complexity. Thus we need to be careful when we interpret model checking results for the learned model. Also, we must be aware that when some property does not hold for the abstract model, and the model checker provides a counterexample, we still need to check whether this counterexample is an actual run of the abstraction of the {\dsut} induced by the mapper. If this is not the case then the counterexample demonstrates that the learned model is incorrect.

\newcommand{\dreqauth}{$hasReqAuth$}
\newcommand{\dvauth}{$validAuthReq$}
\newcommand{\dauthreq}{$authReq$}
\newcommand{\diauth}{$invAuthReq$}
\newcommand{\dopchan}{$hasOpenedChannel$}
\newcommand{\drnewkeys}{$receivedNewKeys$}
\newcommand{\drkex}{$kexStarted$}
\newcommand{\dconnlost}{$connLost$}
\newcommand{\dend}{$endCondition$}

\lstset{%
  escapeinside={(*}{*)},%
}

Before introducing the properties, we mention some basic predicates and conventions we use in their definition. The happy flow in SSH consists in a series of steps: the user (1) exchanges keys, (2) requests for the authentication service, (3) supplies valid credentials to authenticate and finally (4) opens a channel. Whereas step (1) is complex, the subsequent steps can be captured by the simple predicates {\dreqauth}, {\dvauth} and {\dopchan} respectively. The predicates are defined in terms of the output generated at a given moment, with certain values of this output indicating that the step was performed successfully. For example, \textsc{ch\_open\_success} indicates that a channel has been opened successfully. Sometimes we also need the input that generated the output, so as to distinguish this step from other steps. In particular, requesting the authentication service is distinguished from requesting the connection service by \textsc{sr\_auth}. 
To these predicates, we add predicates for valid, invalid and all authentication methods, a predicate for the receipt of \textsc{newkeys} from the server, and receipt of \textsc{kexinit}, which can also be seen as initiation of key (re-) exchange. These last predicates have to be tweaked in accordance with the input alphabet used and with the output the {\dsut} generated (\textsc{kexinit} could be sent in different packaging, either alone, or joined by a different message). Their formulations below are for the OpenSSH server. Finally, {\dconnlost} indicates that connection was lost, and {\dend} is the condition after which higher layer properties no longer have to hold.
\begin{center}
\begin{lstlisting}[basicstyle=\footnotesize]
	(*{\dreqauth}*) := inp=SR_AUTH & out=SR_ACCEPT;
	(*{\dvauth}*) := out=UA_PK_OK | out=UA_PW_OK;
	(*{\dopchan}*) := out=CH_OPEN_SUCCESS;
	(*{\dvauth}*) := inp=UA_PK_OK | inp=UA_PW_OK;
	(*{\diauth}*) := inp=UA_PK_NOK|inp=UA_PW_NOK|inp=UA_NONE;
	(*{\dauthreq}*) := validAuthReq | invalidAuthReq;
	(*{\drnewkeys}*) := out=NEWKEYS | out=KEX31_NEWKEYS;
	(*{\drkex}*) := out=KEXINIT;
	(*{\dconnlost}*) := out=NO_CONN | out=DISCONNECT;
	(*{\dend}*) := kexStarted | connLost;
\end{lstlisting}
\end{center}


Our formulation uses NuSMV syntax. 
%We occasionally rely on past modalities operators such Once (O) and Since (S), which are uncommon, but are supported by NuSMV. 
We also use the weak until operator W, which is not supported by NuSMV, but can be easily defined in terms of the until operator
U and globally operator G that are supported:
$p\,W\,q\, =\, p \,U\, q\, | \, G\, p$.  Many of the higher layer properties we formulate should hold only until a disconnect or a key (re-)exchange happens, hence the definition of the {\dend} predicate. This is because the RFCs don't specify what should happen when no connection exists. Moreover, higher layer properties in the RFCs only apply outside of  rekey sequences, as inside a rekey sequence the RFCs advise implementations to reject all higher layer inputs, regardless of the state before the rekey. 


%In the actual specification, W was replaced by this re-formulation. %Finally, we may define properties which we later use when defining other properties. This feature again isn't supported by NuSMV, hence the properties appear in expanded form in the run specification.

\subsection{Basic characterizing properties}

 %cannot be translated. %Though in practical terms, these results are still translatable, in particular for cases where properties are not met. 
In our setting, a single TCP connection is made and once this connection is lost (e.g.\ because the system disconnects) it cannot be re-established. The moment
a connection is lost is marked by generation of the \textsc{no\_conn} output.  From this moment onwards, the only outputs encountered are the \textsc{no\_conn} output (the {\dmapper} tried but failed to communicate with the {\dsut}), or outputs generated by the {\dmapper} directly, without querying the system. The latter are \textsc{ch\_max} (channel buffer is full) and \textsc{ch\_none} (channel buffer is empty). With these outputs we define Property~\ref{prop:noconn} which describes the ``one connection'' property of our setup.

\begin{property}%[h]
\begin{lstlisting}[basicstyle=\footnotesize]
	G (out=NO_CONN -> 
		G (out=NO_CONN | out=CH_MAX | out=CH_NONE) )
\end{lstlisting}
%\caption{One connection property}
\label{prop:noconn}
\end{property}

Outputs \textsc{ch\_max} and \textsc{ch\_none} are still generated because of a characteristic we touched on in Subsection~\ref{subsec:mapper}. The {\dmapper} maintains a buffer of open channels and limits its size to 1. From the perspective of the {\dmapper}, a channel is open, and thus added to the buffer, whenever \textsc{ch\_open} is received from the learner, regardless if a channel was actually opened on the {\dsut}. In particular, if after opening a channel via \textsc{ch\_open} an additional attempt to open a channel is made, the {\dmapper} itself responds by \textsc{ch\_max} without querying the {\dsut}. This continues until the {\dlearner} closes the channel by \textsc{ch\_close}, prompting removal of the channel and the sending of an actual CLOSE message to the {\dsut} (hence out!=\textsc{ch\_none}). A converse property can be formulated in a similar way for when the buffer is empty after a \textsc{ch\_close}, in which case subsequent \textsc{ch\_close} messages prompt the {\dmapper} generated \textsc{ch\_none}, until a channel is opened via \textsc{ch\_open} and an actual OPEN message is sent to the {\dsut}. Conjunction of these two behaviors forms Property~\ref{prop:channel}.

\begin{property}%[h]
\begin{lstlisting}[basicstyle=\footnotesize]
	(G (inp=CH_OPEN) ->
			X ( (inp=CH_OPEN -> out=CH_MAX)  
				W (inp=CH_CLOSE & out!=CH_NONE) ) ) &
	(G (inp=CH_CLOSE) ->
			X ( (inp=CH_CLOSE -> out=CH_NONE)  
				W (inp=CH_OPEN & out!=CH_MAX) ) ) 
\end{lstlisting}
%\caption{Mapper induced channel property}
\label{prop:channel}
\end{property}

\subsection{Security properties} 
In SSH, upper layer services rely on security guarantees ensured by lower layers. So these services should not be available before the lower layers have completed. For example, the authentication service should only become available \emph{after} a successful key exchange and the seting up of a secure tunnel by the Transport layer, otherwise the service would be running over an unencrypted channel. Requests for this service should therefore not succeed unless key exchange was performed successfully.

Key exchange involves three steps that have to be performed in order but may be interleaved by other actions. Successful authentication necessarily implies successful execution of the key exchange steps. We can tell each key exchange step was successful from the values of the input and output variables. Successful authentication request is indicated by the predicate defined earlier, {\dreqauth}. Following these principles, we define the LTL specification in Property~\ref{prop:sec-trans}, where O is the once operator. Formula $O p$ is true at time $t$ if $p$ held in at least one of the previous time steps $t' \leq t$.


% SR_AUTH_AUTH -> SR_AUTH
% SR_AUTH_CONN -> SR_CONN
% SR_ACCEPT -> SR_ACCEPT


\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
	G ( hasReqAuth -> 
			O ( (inp=NEWKEYS & out=NO_RESP) & 
				O ( (inp=KEX30 & out=KEX31_NEWKEYS) & 
					O (out=KEXINIT) ) ) )
\end{lstlisting}
%\caption{Transport layer security}
\label{prop:sec-trans}
\end{property}

Apart from a secure connection, Connection layer services also assume that the client behind the connection was authenticated. This is ensured by the Authentication layer by means of an authentication mechanism, which only succeeds, and thus authenticates the client, if valid credentials are provided. For the implementation to be secure, there should be no path from an unauthenticated to an authenticated state without the provision of valid credentials. We consider an authenticated state as a state where a channel has been opened successfully, described by the predicate {\dopchan}. Provision of valid/invalid credentials is indicated by the outputs \textsc{ua\_success} and \textsc{ua\_failure} respectively. Along these lines, we formulate this specification by Property~\ref{prop:sec-auth}, where S stands for the since operator. 
Formula $p S q$ is true at time $t$ if $q$ held at some time $t' \leq t$ and $p$ held in all times $t''$ such that $t' < t'' \leq t$.


\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
	G ( hasOpenedChannel ->
		out!=UA_FAILURE S out=UA_SUCCESS )
\end{lstlisting}
%\caption{Authentication layer security}
\label{prop:sec-auth}
\end{property}

\subsection{Key re-exchange properties}
According to the RFC \cite[p. 24]{rfc4254}, re-exchanging keys (or rekeying) (1) is preferably  allowed in all states of the protocol, and (2) its successful execution does not affect operation of the higher layers. We consider two general protocol states, pre-authenticated (after a successful authentication request, before authentication) and authenticated. These may map to multiple states in the learned models. We formalized requirement (1) by
two properties, one for each general state. In the case of the pre-authenticated state,  we know we have reached this state following a successful authentication service request, indicated by the predicate {\dreqauth}. Once here, performing the inputs for rekey in succession should imply success until one of two things happen, the connection is lost(\dconnlost) or we have authenticated. This is asserted in Property~\ref{prop:rpos-pre-auth}. A similar property is defined for the authenticated state.
\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
	G ( hasReqAuth -> 
		X (inp=KEXINIT -> out=KEXINIT & 
			X ( inp=KEX30 -> out=KEX31_NEWKEYS & 
				X (inp=NEWKEYS -> out=NO_RESP) ) ) W 
         (connLost | hasAuth ) ) 
\end{lstlisting}
%\caption{Rekey possible in pre-auth. state}
\label{prop:rpos-pre-auth}
\end{property}
Requirement (2) cannot be expressed in LTL, since in LTL we cannot specify that two states are equivalent.
We therefore checked this requirement directly, by writing a simple script which, for each state $q$ that allows rekeying, checks if the state $q'$ reached after a successful rekey is equivalent to $q$ in the subautomaton that only contains the higher layer inputs.

%Provided we perform successful finalization of a rekey, we remain in a pre-authenticated state until we exit this state, either by losing the connection (suggested by the \textsc{no\_conn} ) or by successful authentication (\textsc{ua\_success}). The latter is described in Property~\ref{prop:rper-pre-auth}. Note that, we can tell we are in a pre-authenticated state if authenticating with a valid public key results in success. W represents the Weak Until operator.
%
%\begin{property}[h]
%\begin{lstlisting}[basicstyle=\footnotesize]
%	G ( hasReqAuth ->
%		( (inp=NEWKEYS & out=NO_RESP & X inp=UA_PK_OK) -> 
%			X out=UA_SUCCESS) 
%		W (out=NO_CONN | out=UA_SUCCESS) )
%\end{lstlisting}
%\caption{Key exchange preserves pre-auth. state}
%\label{prop:rper-pre-auth}
%\end{property}

%\textit{Perhaps this could be merged into one property?}

\subsection{Functional properties}
We formalized and checked several other properties drawn from the RFCs. We found parts of the specification unclear, which sometimes meant that we had to give our own interpretation. A first general property can be defined for the \textsc{disconnect} output. The RFC specifies that after sending this message, a party MUST not send or receive any data \cite[p. 24]{rfc4253}. While we cannot tell what the server actually receives, we can check that the server does not generate any output after sending \textsc{disconnect}. After a \textsc{disconnect} message, subsequent outputs should be solely derived by the {\dmapper}.  Knowing the {\dmapper} induced outputs are \textsc{no\_conn}, \textsc{ch\_max} and \textsc{ch\_none}, we formulate by Property~\ref{prop:trans-disc} to describe
expected outputs after a \textsc{disconnect}.

\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
	G ( out=DISCONNECT -> 
		X G (out=CH_NONE | out=CH_MAX | out=NO_CONN) )
\end{lstlisting}
%\caption{No output after DISCONNECT}
\label{prop:trans-disc}
\end{property}


The RFC states in~\cite[p. 24]{rfc4254} that after sending a \textsc{kexinit} message, a party MUST not send another \textsc{kexinit}, or a \textsc{sr\_accept} message, until it has sent a \textsc{newkeys} message(\drnewkeys). This is translated to Property~\ref{prop:trans-kexinit}.  


\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
	G ( out=KEXINIT -> 
X ( (out!=SR_ACCEPT & out!=KEXINIT) W receivedNewKeys) )
\end{lstlisting}
%\caption{Disallowed outputs after KEXINIT}
%\captionsetup{font=small}
\label{prop:trans-kexinit}
\end{property}

%also to check for sr_conn
The RFC also states \cite[p. 24]{rfc4254} that if the server rejects the service request, ``it SHOULD send an appropriate SSH\_MSG\_DISCONNECT message and MUST disconnect''. Moreover, in case it supports the service request, it MUST send a \textsc{sr\_accept} message. Unfortunately, it is not evident from the specification if rejection and support are the only allowed outcomes. We assume that is the case, and formalize an LTL formula accordingly by Property~\ref{prop:trans-sr}. For a service request (\textsc{sr\_auth}), in case we are not in the initial state, the response will be either an accept (\textsc{sr\_accept}), disconnect (\textsc{disconnect}), or \textsc{no\_conn}, output generated by the {\dmapper} after the connection is lost. We adjusted the property for the initial state since all implementations responded with \textsc{kexinit} which would easily break the property. We cannot yet explain this behavior.
% , with the adjustment that we also allow the mapper induced output \textsc{no\_conn}, which suggests that connection was lost. Additionally, we exclude the initial state from the implication, as it is the only state where a \textsc{kexinit} is generated as a response, which seems to be the default behavior for all implementations.

\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
	G ( (inp=SR_AUTH & state!=s0) ->
	(out=SR_ACCEPT | out=DISCONNECT | out=NO_CONN )))
\end{lstlisting}
%\caption{Allowed outputs after SR\_ACCEPT}
\label{prop:trans-sr}
\end{property}



%\textit{Could strengthen this so we check that it disconnects (DISCONNECT) after the first rejected service request}

The RFC for the Authentication layer states in~\cite[p. 6]{rfc4252} that if the server rejects the authentication request, it MUST respond with a \textsc{ua\_failure} message. Rejected requests are suggested by the predicate {\diauth}. In case of requests with valid credentials (\dvauth), a \textsc{ua\_success} MUST be sent only once. While not explicitly stated, we assume this to be in a context where the authentication service had been successfully requested, hence we use the {\dreqauth} predicate. We define two properties, Property~\ref{prop:auth-pre-ua} for behavior before an \textsc{ua\_success}, Property~\ref{prop:auth-post-ua-strong} for behavior afterward. For the first property, note that (\dreqauth) may hold even after successful authentication, but we are only interested in behavior between the first time (\dreqauth) holds and the first time authentication is successful (out=\textsc{ua\_success}), hence the use of the O operator. As is the case with most higher layer properties, the first property only has to hold until the end condition holds (\dend), that is the connection is lost (\dconnlost) or rekey was started by the {\dsut} (\drkex).
%Indeed, before reaching this state, implementations replied with \textsc{unimplemented}. 
\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
G ( (hasReqAuth & !O out=UA_SUCCESS) ->
	(invalidAuthReq -> out=UA_FAILURE) 
		W (out=UA_SUCCESS | endCondition) )
\end{lstlisting}
%\caption{Invalid requests prompt UA\_FAILURE }
\label{prop:auth-pre-ua}
\end{property}

\begin{property}
\vspace{-4mm}
\begin{lstlisting}[basicstyle=\footnotesize]
G ( out=UA_SUCCESS -> X G out!=UA_SUCCESS)
\end{lstlisting}
%\caption{UA\_SUCCESS is sent at most once}
\label{prop:auth-post-ua-strong}
\end{property}

In the same paragraph, it is stated that authentication requests received after a \textsc{ua\_success} SHOULD be ignored. This is a weaker statement, and it requires that all authentication messages (suggested by {\dauthreq}) after a \textsc{ua\_success} output should prompt no response from the system(\textsc{no\_resp}) until the end condition is true. The formulation of this statement shown in Property~\ref{prop:auth-post-ua}. 
\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
	G ( out=UA_SUCCESS -> 
		X ( ( authReq-> out=NO_RESP ) W endCondition ) )
\end{lstlisting}
%\caption{Silence after UA\_SUCCESS}
\label{prop:auth-post-ua}
\end{property}
%\textit{Perhaps this could also be merged into one property?}

The Connection layer RFC states in \cite[p. 9]{rfc4254} that on receiving a \textsc{ch\_close} message, a party MUST send back a \textsc{ch\_close}, unless it had already sent this message for the channel. The channel must have been opened beforehand (\dopchan) and the property only has to hold until the end condition holds or the channel was closed (\textit{out=CH\_CLOSE}).  We formulate Property~\ref{prop:conn-close} accordingly. 

\begin{property}
\begin{lstlisting}[basicstyle=\footnotesize]
G ( hasOpenedChannel -> 
	( (inp=CH_CLOSE) -> (out=CH_CLOSE) ) 
	W ( endCondition | out=CH_CLOSE) )
\end{lstlisting}
%\caption{CH\_CLOSE response on CH\_CLOSE request}
\label{prop:conn-close}
\end{property}


%A problem was that the specification for each layer seemed to only consider messages specific to that layer. In particular, it didn't consider disconnect messages, that may be sent at any point in the protocol. For the Transport Layer, the RFC states that after sending a \textsc{kexinit} message, a party MUST not send \textsc{service\_request} or \textsc{sr\_accept} messages, until it has sent a \textsc{newkeys} message. This is translated to the LTL.

%On the same page, the RFC also states that in case the server rejects a service request, it should send an appropriate \textsc{disconnect} message. Moreover, in case it supports the service request, it MUST sent a \textsc{sr\_accept} message. If

%The Connection Layer RFC states that upon receiving a CH_CLOSE message, a side should send back a CH\_CLOSE message, unless it has already sent this message for the channel. This of course, ignores the case when a side disconnects, in which case a CH_CLOSE would no longer have to be issued. 
\newcommand{\dt}{\color{green}\checkmark}
\newcommand{\dfce}[1]{\color{red}X}
\newcommand{\df}{\color{red}X}


\subsection{Model checking results}
Table~\ref{tab:mcresults} presents model checking results. Crucially, the security properties hold for all three implementations. We had to slightly adapt our properties for Bitvise 
as it buffered all responses during rekey (incl. \textsc{UA\_SUCCESS}).
In particular, we used {\dvauth} instead of \textit{out=UA\_SUCCESS} as sign of successful authentication. 

%\begin{center}\small
%	\centering
\begin{table}[h!]
	\centering
	\small
	\caption{Model checking results}
	\begin{tabular}{| l | l | c | c |c | c |}
		\hline 
		 %         &   					 																	&		          & 		\multicolumn{3}{c|}{\emph{SSH Implementation}}\\ \cline{4-6} 					
							&   Property 	 																	&	Key word &\tiny{OpenSSH}	                 		& \tiny{Bitvise}              & \tiny{DropBear} \\ \hline 
		Security  &   Trans. 		 																	&		          & \dt  		                 		& \dt                  & \dt                					  \\ \cline{3-6} 
		    			&   Auth.      																	&		          & \dt	 		                 		& \dt                  & \dt                					  \\ \hline 
		Rekey 		&   Pre-auth.  													    &		          & \dfce{sends unimpl}     		& \dt                  & \dt                					  \\ \cline{3-6}
		    			&   Auth. 															    &		          & \dt                      		& \dfce{disc for kex}  & \dt                					  \\ \hline 
		Funct.&   Prop. ~\ref{prop:trans-disc}								& MUST        & \dt  		                 		& \dt                  & \dt                					  \\ \cline{3-6} 
							&   Prop.~\ref{prop:trans-kexinit}							& MUST        & \dt	                 		    & \dt                  & \dt                					  \\ \cline{3-6} 
		    			&   Prop.~\ref{prop:trans-sr}									&	MUST        & \dfce{sends unimpl}*   			& \dfce{kex no resp}   & \dt                					  \\ \cline{3-6}
		    			&   Prop.~\ref{prop:auth-pre-ua} 								&	MUST        & \dt                   			& \dt                  & \dt                					  \\ \cline{3-6}
							&   Prop.~\ref{prop:auth-post-ua-strong}				&	MUST        & \dt		                 			& \dt                  & \dt												   \\ \cline{3-6}
							&   Prop.~\ref{prop:auth-post-ua} 							&	SHOULD      & \dfce{sends unimpl}*   			& \dfce{sends unimpl}* & \dt                 						\\ \cline{3-6}
		    			&   Prop.~\ref{prop:conn-close}  								&	MUST        & \dt		                 			& \dt                  & \dfce{sends CH\_EOF}						\\ \hline 		
	\end{tabular}
	\label{tab:mcresults}
	%\vspace{-5mm}
\end{table}
%\end{center}

Properties marked with '*' did not hold because implementations chose to send \textsc{unimpl}, instead of the output suggested by the RFC. As an example,
after successful authentication, both Bitvise and OpenSSH respond with \textsc{unimpl} to further authentication requests, instead of being silent, violating
Property~\ref{prop:auth-post-ua}. Whether the alternative behavior adapted is acceptable is up for debate. Certainly the RFC does not suggest it, though it
does leave room for interpretation.

DropBear is the only implementation that allows rekeying in both general states of the protocol. DropBear also satisfies all Transport and Authentication layer specifications, however,
problematically, it violates the property of the Connection layer. Upon receiving \textsc{ch\_close}, it responds by \textsc{ch\_eof} instead of \textsc{ch\_close}, not respecting 
Property~\ref{prop:conn-close}. %Moreover, the output \textsc{ua\_success} can be generated multiple times, violating both Properties ~\ref{prop:auth-post-ua-strong} and
 %~\ref{prop:auth-post-ua}.

%, though key exchange is strangely not universally permitted, while some of the functional properties described are not met.
