\chapter{Results}\label{results}
This chapter discusses the state machines inferred by our testing setup. The transport layer, user authentication layer and connection layer will be discussed in Sections~\ref{results-trans}-\ref{results-conn}. For each layer, we will discuss the state machines in order of increasing complexity. A few concluding observations will be made in Section~\ref{results-rfcs}.

\section{Transport layer}\label{results-trans}
The transport layer is arguably the most interesting layer from a security perspective because it involves parameter negotiation and key exchange. The SUTs were queried with the alphabet described in Table~\ref{trans-alphabet}. The inferred state machines discussed in this section reveal major implementation differences. The majority of the observed differences has one of the following causes:
\begin{enumerate}\label{majorcategories}
\item \textit{Rekeying} during the first execution of the transport layer is either allowed or not allowed.
\item Responses to \textit{authentication service request} while in key exchange are handled differently. The RFC explicitly disallows sending an acceptance response to any service request while in key re-exchange~\cite[p. 19]{rfc4253}. Some developers have implemented this requirement by buffering responses: acceptance responses are kept back until key re-exchange has completed. Other developers have chosen to simply discard these responses during rekeying. 
\item The presence of \textit{unresponsive and superfluous states}. SUTs in an unresponsive state ignore all incoming messages. Superfluous states are states which serve no real purpose: if they would be removed, the layer would still be working properly\footnote{It is hard to give a formal definition of a superfluous state, since a reader's interpretation is needed to see whether a state serves a \textit{useful} purpose.}. 
\end{enumerate}

Learning statistics for the transport layer are shown in Table~\ref{trans-stats}. Note that equivalence queries have not been saved to the query log, and are therefore not included in the statistics tables.
\clearpage
All inferred state machines in this section are secure with regard to the security definition given in Section~\ref{ssh-run-trans}.

\begin{table}[!ht]
\centering
\begin{tabular}{lrrr}
\textbf{SUT} & \textbf{Traces} & \textbf{Queries} & \textbf{Time ([h:]m:s)\tablefootnote{Time per query differs because of the differences in time-outs. In general, time-outs for SUTs running on the same machine as the learner could be lower than time-outs for remote or virtualized servers. }} \\
OpenSSH & 895 & 5584 & 16:57 \\
DropBear & 562 & 2634 & 8:38 \\
Bitvise & 766 & 3091 & 16:03 \\
PowerShell & 866 & 5413 & 19:14 \\
Tectia & 1224 & 8203 & 1:01:05 \\
CiscoSSH & 522 & 2689 & 51:41 \\
\end{tabular}
\caption{Statistics for the transport layer extracted from the query log.}
\label{trans-stats}
\end{table}

\clearpage
\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.8\textwidth]{machines/openssh-localhost-L1.pdf}}
  \caption{Inferred state machine of the transport layer for OpenSSH 6.9p1-2.}
  \label{fig:opensshl1}
\end{figure}

OpenSSH (Figure~\ref{fig:opensshl1}) implements a simple state machine. It does not allow rekeying in this phase of the protocol, and therefore does not need a rekeying response buffer. OpenSSH is extremely liberal when it comes to the parameter negotiation: the \textsc{kexinit} message need not to be sent at all. OpenSSH seems to do a proper job when it comes to guessing all of the used parameters for the client-to-server connection. OpenSSH's developers have been notified of this issue, but have not (yet) shed light on why they chose to implement such a liberal acceptance policy during parameter negotiation. 

The parameter negotiation behaviour in OpenSSH is in line with Postel's law, which has been formulated in an early version of the TCP standard~\cite[p. 21]{rfc760} and boils down to ``be conservative in what you send, be liberal in what you accept from others''. It has been argued that this approach is unwise in a security-sensitive context~\cite{Poll2011Rigorous}. Interesting enough, OpenSSH does clearly not follow Postel's law when it comes to rekeying, since it is one of the few clients that disallow rekeying at this stage of the protocol. 

\clearpage
\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.8\textwidth]{machines/cisco-localhost-L1.pdf}}
  \caption{Inferred state machine of the transport layer for CiscoSSH 1.25.}
  \label{fig:ciscol1}
\end{figure}

CiscoSSH's state machine (Figure~\ref{fig:ciscol1}) resembles OpenSSH's, although it does requires a \textsc{kexinit} message from the client before starting the key exchange. In other words, a transition from the ``initial'' state to the ``prekex'' state is not possible without mutually exchanging preferred parameters. The \textsc{guessinit} is not supported. 

Contrary to OpenSSH, the user authentication service can only be requested once, and the connection is closed on subsequent requests. Although the RFC does not mention limiting the number of \textsc{sr\_auth} messages, this is a more restrictive implementation.

Just like OpenSSH's state machine, CiscoSSH does not allow rekeying in this stage of the protocol. Cisco's developers have not responded to inquiries as to why they chose this behaviour. 

\clearpage
DropBear's state machine (Figure~\ref{fig:dropbearl1}) also resembles OpenSSH's, but allows rekeying after the initial key exchange has completed. 

Just like OpenSSH and CiscoSSH, DropBear does not buffer \textsc{accept} messages. In reply to my inquiry on the reason behind this, DropBear's developer Matt Johnston stated that according to him, buffering these messages is not allowed by the RFCs. Indeed, the description  in~\cite[p. 19]{rfc4253} is ambiguous, and can be interpreted as a requirement to temporary withhold these messages, \textit{or} as a requirement to discard them. 

\begin{figure}
  \noindent\makebox[\textwidth]{\includegraphics[width=0.8\textwidth]{machines/dropbear-localhost-L1.pdf}}
  \caption{Inferred state machine of the transport layer for Dropbear 2014.65-1.}
  \label{fig:dropbearl1}
\end{figure}

\clearpage

\begin{figure}
  \noindent\makebox[\textwidth]{\includegraphics[width=1.15\textwidth]{machines/bitvise-vm-L1.pdf}}
  \caption{Inferred state machine of the transport layer for Bitvise 6.45.}
  \label{fig:bitvisel1}
\end{figure}
Bitvise's state machine (Figure~\ref{fig:bitvisel1}) stops responding when a \textsc{newkeys} directly follows parameter negotiation. We have tested this unresponsive state for a denial-of-service scenario, but it seems that the ``unresp'' state closes connections after one minute. The \textsc{debug} message the SUT sends on a second authentication request results in a separate state ``preauth''.

In response to an inquiry about the unresponsive state, Bitvise developers started an investigation which led to the conclusion that ``the reason for the unresponsive state is in our asynchronous, component-based, message-passing architecture; and the way we use this to implement a kex-handler-agnostic transport layer''. Although Bitvise agrees that an error message would have been better in this case, they also note that ``other mechanisms, such as various time-outs, should continue to operate in this circumstance, so it seems an error message would be the only benefit.'' They conclude that the unresponsive state results from an ``architectural limitation'', but it does little harm. 
 
\clearpage
\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=1.3\textwidth]{machines/powershell-vm-L1.pdf}}
  \caption{Inferred state machine of the transport layer for PowerShell 6.0.5732.}
  \label{fig:powershelll1}
\end{figure}

The state machine of PowerShell (Figure~\ref{fig:powershelll1}) is more complex because of its buffering behaviour and the existence of multiple superfluous states. The grey superfluous states are caused by PowerShell's odd interpretation of the \textsc{sr\_auth} and \textsc{sr\_conn} messages. As can be seen in Figure~\ref{fig:powershelll1}, these messages can cause state changes but frequently do not result in a response. The resulting ``preclosed''-states do not seem to serve any purpose. Other SUTs simply close the connection upon unexpectedly receiving these messages. 

Recall that the asterisk in \textsc{accept*} is used to indicate an arbitrary non-zero number of identical responses to a single query. These responses originate from buffered responses to \textsc{sr\_auth} messages during key re-exchange. 

PowerShell's developers responded to these findings by stating that they would further investigate the particularities when time allows. 

\clearpage

\begin{sidewaysfigure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=1.0\textwidth]{machines/tectia-vm-L1-filtered-ignore.pdf}}
  \caption{Inferred state machine of the transport layer for Tectia 6.4.12.353.}
  \label{fig:tectial1}
\end{sidewaysfigure}

\clearpage
Tectia's state machine (Figure~\ref{fig:tectial1}), just like DropBear's, contains an unresponsive state when \textsc{newkeys} is sent too early. Whether this is caused by the same architectural limitation cannot be answered, since Tectia's developers have not taken the opportunity to respond to the results. 

Tectia accepts the \textsc{newkeys} message before renewed keys have actually been exchanged. The SUT's extremely liberal rekey sequence acceptance policy in combination with buffering behaviour results in a tangled web of interrelationships. Actual rekeying is performed as soon as a \textsc{kexinit}, \textsc{kex30} and a \textsc{newkeys} have been received, regardless of the order. While this does not directly lead to a vulnerability, this behaviour is neither wise nor allowed by the RFC, which clearly state that ``key exchange ends by each side sending an \textsc{newkeys} message''~\cite[p. 21]{rfc4253}.

Given this behaviour, it is likely that the state machine has been implemented as a combination of variables which combinedly form the machine's state. A state machine could also be implemented using a single state variable. It should be noted that no single method is superior, although the multi-variable approach can result in an cluttered state machine representation with many seemingly unnecessary states.

Note that the majority of states in Figure~\ref{fig:tectial1} have not been marked as superfluous. Although the state machine implementation would be better off with fewer states, all of the states (besides the ``unresp'') allow recovering to a state like ``kexed'' or ``keyed''. These states are the result of the aforementioned implementation decisions, but are strictly speaking not superfluous. 

\clearpage
\subsection{Concluding remarks on the transport layer}
The inferred transport layer state machines differ substantially. While causes for these changes fall into three major categories described on page~\pageref{majorcategories}, many more subtle variances can be observed in both structure of the state machine and the used response messages. Differences in implemented response messages are especially notable when considering unexpected messages: some SUTs send \textsc{unimpl}, others send \textsc{no\_resp}, \textsc{discon}, or simply immediately close the connection. Querying with the \textsc{guessinit} message did not result in peculiar behaviour, and all SUTs expect CiscoSSH support it.

All state machines are secure, since none of the state machines show a path to the authentication service request without a proper exchange of cryptographic keys\footnote{The security definition can be found in Section~\ref{ssh-run-auth}.}.

From a security perspective, a liberal message acceptance policy in the key exchange phase is undesirable. OpenSSH has a liberal policy when it comes to parameter negotiation because it also accepts other messages than \textsc{kexinit}. Tectia allows rekeying in arbitrary message order. This liberal behaviour could easily lead to an obscure combination of states in which errors are easily introduced and hard to detect. Other SUTs are more restrictive what kind of messages they accept. 

Both Tectia and PowerShell buffer \textsc{accept} messages during rekeying, resulting in extra states with \textsc{accept*} transitions. Buffering \textsc{accept} messages seems to be implied by the specifications, which state that a client or server ``must not send any messages other than'' the ones provided on~\cite[p. 19]{rfc4251}. Given that key re-exchange may take place at any given moment, it is likely that the RFC authors meant holding back other messages -such as \textsc{accept}- rather than discarding them. However, the specifications remain ambiguous and are interpreted differently by different readers. This is directly reflected by the response from DropBear's developer, but also observable by looking at how developers have translated the RFCs to state machines. 

It is hard to say which state machine is the ``best''. This depends on one's interpretation of the RFCs, and on whether the most compliant implementation is always preferable. We would recommend developers to not allow rekeying and not buffer \textsc{accept} messages. Choosing a simple state machine over a strict implementation of specifications seems sensible in this error-prone stage of the protocol, since it results in a simpler state machines which, in turn, allows for fewer state-related bugs. For this reason, we would argue that CiscoSSH's state machine (Figure~\ref{fig:ciscol1}) is preferable. 

\clearpage

\section{User authentication layer}\label{results-auth}
Inferring state machines for the user authentication layer proved to be relatively easy compared to inferring the transport layer because the implemented state machines were less complex. Although all SUTs implement a secure user authentication layer with regard to the security definition given in Section~\ref{ssh-run-auth}, various differences can be observed.

The used alphabet consists of the \textsc{rekey} query\footnote{More information on the \textsc{rekey} message is available in Section~\ref{layers-individual}.} and the queries described in Table~\ref{auth-alphabet}. Throughout this chapter, the \textsc{keyok} response message was used as a shorthand for the sequence of the three expected result messages (\textsc{kexinit; kex31; newkeys}) in a key re-exchange. 

The initial state for this layer is ``unauthed''. In order to reach this state, the sequence \textsc{kexinit; kex30; newkeys; sr\_auth} has been executed in between every trace. This performs a key exchange and requests the user authentication service. Besides the atomic \textsc{rekey} message, no other transport layer messages are used in this layer in order to keep learning times feasible. Resulting learning statistics for the authentication layer can be found in Table~\ref{auth-stats}. 

\begin{table}[!ht]
\centering
\begin{tabular}{lrrr}
\textbf{SUT} & \textbf{Traces} & \textbf{Queries} & \textbf{Time (m:s)} \\
OpenSSH & 78 & 618 & 4:38 \\
DropBear & 107 & 969 & 9:34 \\
Bitvise & 103 & 993 & 6:57 \\
PowerShell & 87 & 672 & 6:39 \\
Tectia & 95 & 728 & 11:17 \\
CiscoSSH & 105 & 758 & 3:44 \\
\end{tabular}
\caption{Statistics for the authentication layer extracted from the query log.}
\label{auth-stats}
\end{table}

\clearpage
\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.75\textwidth]{machines/l2_combined.pdf}}
  \caption{Inferred state machine of the user authentication layer for DropBear 2014.65-1, Tectia 6.4.12.353 and PowerShell 6.0.5732 (left) and Bitvise 6.45 (right).}
  \label{fig:l2_combined}
\end{figure}

The structure of the state machines for Bitvise, Tectia, DropBear and PowerShell (Figure~\ref{fig:l2_combined}) are almost identical and arguably as simple as the authentication layer could theoretically be. Their state machines have two states, and rekeying is allowed during the entire protocol.

Tectia sends a \textsc{ua\_banner} ~\cite[p. 7]{rfc4252} as soon as the SUT connects, which can be used to inform connecting clients of relevant (legal) information. This message has been filtered from the representation in Figure~\ref{fig:l2_combined}, because it would falsely gives the impression that this message is a response to the first query, while it might have already been sent\footnote{Section~\ref{challenging-multiple} provides more information on this behaviour.}. 

\clearpage
\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.55\textwidth]{machines/openssh-localhost-L2.pdf}}
  \caption{Inferred state machine of the user authentication layer for OpenSSH 6.9p1-2.}
  \label{fig:opensshl2}
\end{figure}

OpenSSH's state machine (Figure~\ref{fig:opensshl2}) does not allow rekeying until user authentication has been completed. Besides that, the state machine is identical to Bitvise's state machine. OpenSSH and Tectia do not allow requesting authentication with a changed username. In other words: subsequent authentication requests have to involve the same user. This behaviour is not defined by the RFCs, and initially led our mapper to infer incorrect state machines. OpenSSH's debug logs revealed this issue, after which our mapper has been altered to use identical usernames. We do not know the reason for this implementation decision, and the OpenSSH developers did not elaborate on it.

As soon as a SUT successfully authenticates, OpenSSH transmits a global request featuring their own protocol extension (\texttt{hostkeys-00@openssh.com}), which is used to inform clients of all the server's protocol host keys\footnote{A complete list of OpenSSH's deviations and extensions can be found on \\ \url{https://anongit.mindrot.org/openssh.git/plain/PROTOCOL}}. This is a OpenSSH-specific extension using the ``\texttt{@}'' notation as defined in~\cite[p. 9]{rfc4250}. 

\clearpage
\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.55\textwidth]{machines/cisco-localhost-L2.pdf}}
  \caption{Inferred state machine of the user authentication layer for CiscoSSH 1.25.}
  \label{fig:cisco}
\end{figure}

Just like OpenSSH, CiscoSSH does not allow rekeying until the user has been successfully authenticated. As stated in Section~\ref{suts}, public key authentication is not supported in the tested version op CiscoSSH. Just like Cisco's state machine in the transport layer, its inferred state machine for the authentication layer is the most restrictive one: unexpected authentications requests in the ``authed'' state result in a closed connection.

\subsection{Concluding remarks on the user authentication layer}
The state machines for the user authentication layer are less complex than their transport layer counterparts. The state machines differ in two aspects: whether they allow rekeying in an ``unauthed'' state and how implementations deal with authentication attempts in the ``authed'' state. OpenSSH and CiscoSSH do not allow rekeying for unauthenticated users. Other SUTs seem to correctly preserve their state after rekeying. Most implementations send no response or an \textsc{unimpl} message when faced with unexpected authentication attempts, while CiscoSSH immediately closes the connection. Furthermore, OpenSSH and Tectia do not allow to switch usernames between subsequent requests.

All inferred state machines implement a secure state machine: none of the SUTs allow reaching an authenticated state without providing correct credentials. Furthermore, none of the SUTs show unresponsive or superfluous states\footnote{The security definition can be found in Section~\ref{ssh-run-auth}.}. Bitvise's state machine (Figure~\ref{fig:l2_combined}) most closely follows the RFCs. However, just like in the transport layer, developers might have good reasons to disable rekeying until user authentication. 

\clearpage
\section{Connection layer}\label{results-conn}
The connection layer allows a user to request different processes over a single SSH connection\footnote{More information on the connection layer is available in Section~\ref{ssh-run-conn}.}. Querying the SUTs with the alphabet described in Table~\ref{conn-alphabet} and the \textsc{rekey} query resulted in the learning statistics shown in Table~\ref{conn-stats}. The initial state for this layer is the ``no\_channel'' state. The key exchange sequence (\textsc{kexinit; kex30; newkeys; sr\_auth}) is followed by successful authentication (\textsc{ua\_pw\_ok}) in between every trace in order to reach this state. 

\begin{table}[!ht]
\centering
\begin{tabular}{lrrr}
\textbf{SUT} & \textbf{Traces} & \textbf{Queries} & \textbf{Time (m:s)} \\
OpenSSH & 540 & 5115 & 10:50 \\
DropBear & 490 & 4595 & 9:55 \\
Bitvise & 294 & 2315 & 6:43 \\
PowerShell & 289 & 2343 & 6:39 \\
Tectia & 160 & 1109 & 6:00 \\
CiscoSSH & 264 & 1810 & 5:40 \\
\end{tabular}
\caption{Statistics for the connection layer extracted from the query log.}
\label{conn-stats}
\end{table}

Looking at the RFC~\cite{rfc4254}, we notice that it gives few clues on how to handle unexpected messages. Compared with the other layers, the connection layer RFC focusses more on what is allowed rather than what is disallowed. A possible explanation could be that this layer is less security critical, since it does not involve exchanging keys or passwords. We would argue that the connection layer is, more than the other layers, underspecified. For example, it does not specify how to deal with requesting a second process over a single channel (\textsc{ch\_request\_pty}) or sending data after an end-of-file message (\textsc{ch\_eof}). We will see that each implementation takes it own approach in these cases.
\clearpage

\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.95\textwidth]{machines/l3_combined.pdf}}
  \caption{Inferred state machine of the connection layer for Tectia 6.4.12.353 (left), PowerShell 6.0.5732 (middle) and CiscoSSH 1.25 (right).}
  \label{fig:combinedl3}
\end{figure}

PowerShell, Tectia and CiscoSSH implement a fairly simple state machine (Figure~\ref{fig:combinedl3}). They support rekeying and requesting a terminal, and do not respond to other messages at all. The latter is in line with the RFC, which does not specify a response to \textsc{ch\_data}, \textsc{ch\_edata}, \textsc{ch\_window\_adjust} or \textsc{ch\_eof}. 

PowerShell and Tectia have a rather liberal acceptance policy in two aspects. Firstly, they accept multiple terminal emulation requests over a single channel. Secondly, they preserve their state when receiving an end-of-file message (\textsc{ch\_eof}). They could also have chosen to take measures (for example, close the connection) if a client sends data after \textsc{ch\_eof}, but they did not.  

PowerShell and CiscoSSH do not support opening multiple channels\footnote{Technically, our learning setup can only infer that these SUTs do not support closing and subsequently opening another channel, since our mapper does not support simultaneously opened channels.}, and jump to an unresponsive or closed state as soon as a channel has been closed. This deviation from the RFCs does no justice to the intention of the connection layer, which is to multiplex multiple channels into a single connection~\cite[p. 5]{rfc4254}. Developers of PowerShell and CiscoSSH did not respond to queries about this bug, but it could be a deliberate decision to implement only part of the specification. After all, both SUTs are marketed to perform one specific task only: PowerShell's selling point is providing the Windows PowerShell terminal, while CiscoSSH provides management access to networking appliances. 

\clearpage

\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.75\textwidth]{machines/bitvise-vm-L3.pdf}}
  \caption{Inferred state machine of the connection layer for Bitvise 6.45.}
  \label{fig:bitvisel3}
\end{figure}

The state machine of Bitvise (Figure~\ref{fig:bitvisel3}) is different from the three aforementioned because it does not respond to any connection-layer messages after \textsc{ch\_eof}. Although the RFC does not specify what to do when data is received after an end-of-file, not responding to this data seems more in line with the intention of the client sending such a message. After all, if a server has to anticipate on receiving more data after an end-of-file message, the message serves no real purpose. 
\clearpage

\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=1.0\textwidth]{machines/dropbear-localhost-L3.pdf}}
  \caption{Inferred state machine of the connection layer for Dropbear 2014.65-1.}
  \label{fig:dropbearl3}
\end{figure}

DropBear's state machine (Figure~\ref{fig:dropbearl3}) looks more complex at first sight, which is caused by two implementation decisions. Firstly, just like Bitvise, DropBear does not respond on any connection layer messages when an end-of-file has been issued. Secondly, it allows only one terminal emulation request per channel. Non-compliance with these two restrictions results in a closed connection. 
\clearpage
\begin{figure}[!ht]
  \noindent\makebox[\textwidth]{\includegraphics[width=0.85\textwidth]{machines/openssh-localhost-L3.pdf}}
  \caption{Inferred state machine of the connection layer for OpenSSH 6.9p1-2.}
  \label{fig:opensshl3}
\end{figure}

OpenSSH (Figure~\ref{fig:opensshl3}) also closes the connection on a second terminal emulation request for the same channel. Furthermore, its state machine seems to allow multiple channels, but only until as long as no non-service request query has been received yet. As soon as another query has been received, it changes to the ``has\_commands'' state, after which the closing of a channel results in connection termination. The same behaviour can be observed after a terminal emulation request has been accepted, resulting in the ``has\_commands\_pty'' state. This behaviour is rather strange and cannot be explained using the RFC or OpenSSH's debug logs. As a consequence of this behaviour, OpenSSH fails to close a channel after rekeying, and instead closes the entire connection. OpenSSH developers have been notified, but have so far not provided an explanation.

\subsection{Concluding remarks on the connection layer}
From a state machine security perspective, the transport and user authentication layer are more interesting than the connection layer. The connection layer could contain security vulnerabilities, but it is unlikely that a vulnerability can be revealed by interpreting a SUT's state machine. 

PowerShell and CiscoSSH do not seem to support multiple channels. Rekeying is allowed by all SUTs, but for OpenSSH seems to result in closing the entire connection upon receiving a \textsc{ch\_close}. This indicates a state machine-related bug.

It comes as no surprise that the fewer constructions are explicitly disallowed by the RFCs, the wider the ranger of resulting state machines. The RFCs for the connection layer almost entirely lack \textit{explicit} description of state-related behaviour. Even with the limited number of requests\footnote{We restricted ourselves to terminal emulation, as explained in Section~\ref{alphabet}.}. we employed when inferring state machines for this layer, various ambiguities are revealed. The \textsc{ch\_eof} is exemplary, since the RFC only specify that it should be sent ``when a party will no longer send more data to a channel''~\cite[p. 9]{rfc4254}. Why this is useful and what the consequence is if data is sent afterwards is not explained.

Of the wide range of state machines, DropBear's interpretation of the RFCs is arguably preferable because it disallows dubious service request and end-of-file constructions. 

\section{State machines and RFCs}\label{results-rfcs}
Many differences can be observed when comparing the inferred state machines. In fact, only in the authentication layer did we observe two identical state machines. Fingerprinting an unknown SSH server therefore seems to be a trivial task. One of the major reasons for these observed variances is underspecification in the RFCs. Underspecification can be the result of deliberately allowing multiple options, having ambiguously protocol descriptions, or not discussing certain possibilities at all. 

Underspecification accounts for at least three observed differences amongst the inferred state machines: the buffering behaviour during key re-exchange, dealing with multiple services over a single channel and different handling of \textsc{ch\_eof} messages. Other differences seem to be caused by explicit choices which are not part of the RFCs, or are simply caused by programming errors. 

We strongly believe that if the SSH protocol authors would have sketched a conceptual state machine while drafting the standards, the resulting RFCs would have been more clear, and inferred state machines would show fewer differences. Sketching conceptual state machines forces one to explicitly think about the impact of a protocol rule on the state machine. This might lead to omitting certain features (key re-exchange during transport layer would be a good candidate) or messages (the \textsc{ch\_eof} message can be omitted without any consequence). 

Adding a reference state machine to the RFCs (ASCII-based or otherwise) has advantages. It would allow for easier interpretation by developers, and might result in simpler and more consistent state machine implementations. This might, in turn, ensure better compatibility and leave less room for state-related security vulnerabilities. 

Although adding a reference state machine to the SSH protocol architecture would be a significant improvement, appending state machines might not be suitable for every protocol. It implies that the protocol behaves (almost) Mealy-machine compliant. While non-deterministic Mealy machines allow for more flexible constructions (such as the ones made with the ``MAY'' keyword), RFCs which allow a wide range of possible state machines might become hard to interpret. It is, however, our belief that adding a state machine to current and future RFCs would generally have added value.

Whether a Mealy machine is the best way to depict a state machine is open for debate. In general, we feel that one of the major advantages of Mealy machine representation is that it provides an interpretable overview in the blink of an eye. Other formalisms, such as register automata or the formalism used in~\cite{Poll2011Rigorous} can be harder to interpret but do provide more expression power (for buffers and order of messages respectively). In the end, our choice for Mealy machines was based on a rather practical limitation: these are the only machines L* can infer. 
