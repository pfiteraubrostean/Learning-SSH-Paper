\section{Secure shell}\label{ssh} 
The development of SSHv1 started in 1995 when Finnish researcher Tatu Ylönen drafted the first version of the software and its protocol at the Helsinki University of Technology. It was later standardized by the Internet Engineering Task Force, but quickly succeeded by SSHv2 because it contained design flaws~\cite{FutoranskyAttack} that could not be fixed without losing backwards compatibility. 

SSH follows a client-server paradigm consisting of three components (Figure~\ref{fig:sshcomponents}):
\begin{itemize}
\item The \textit{transport layer protocol} (RFC 4253~\cite{rfc4253}) forms the basis for any communication between a client and a server. It provides confidentiality, integrity and server authentication as well as optional compression.
\item The \textit{user authentication protocol} (RFC 4252~\cite{rfc4252}) is used to authenticate the client to the server.
\item The \textit{connection protocol} (RFC 4254~\cite{rfc4254}) allows the encrypted channel to be multiplexed in different channels. These channels enable a user to run multiple processes, such as terminal emulation or file transfer, over a single SSH connection. 
\end{itemize}

Different layers are identified by their message numbers. These message numbers will form the basis of the state fuzzing. The SSH protocol is especially interesting because outer layers do not encapsulate inner layers. This means that different layers can interact. One could argue that this is a less systematic approach, in which a programmer is more likely to make state machine-related errors.

\begin{figure}[!hb]
\centering
  \includegraphics[width=0.85\linewidth]{imgs/SSH_protocols.png}
  \caption{SSH protocol components running on a TCP/IP stack.}
  \label{fig:sshcomponents}
\end{figure}

\subsection{Transport layer}\label{ssh-run-trans} 
SSH provides end-to-end encryption based on pseudo-random keys which are securely negotiated during key exchange. The negotiation begins with the \textsc{kexinit} message, which exchanges the preferences for negotiable parameters. Subsequently, the actual key exchange using the negotiated algorithm takes place, using \textsc{kex30} and \textsc{kex31} messages using Diffie-Hellman as negotiated key exchange algorithm. The keys are used from the moment the \textsc{newkeys} command has been issued by both parties. A subsequent \textsc{sr\_auth} requests the authentication service. We consider an transport layer state machine secure if there is no path from the initial state to the point where the authentication service is invoked without exchanging and employing cryptographic keys.

\begin{figure}[!hb]
  \includegraphics[width=0.5\textwidth]{imgs/hf-trans.pdf}
  \caption{The happy flow for the transport layer.}
  \label{fig:sshcomponents}
\end{figure}

\subsection{User authentication layer}\label{ssh-run-auth} 
The RFC defines four authentication methods (password, public-key, host-based and none), which are all sent using the same message number~\cite[p. 8]{rfc4252}. The authentication request includes a user name, service name and authentication data, which includes both the authentication method as well as the data needed to perform the actual authentication, such the password or public key. The happy flow for this layer is defined as the sequence that results in a successful authentication. The queries \textsc{ua\_pw\_ok} and \textsc{ua\_pk\_ok} achieve this for respectively password or public key authentication. 
We consider a user authentication layer state machine secure if there is no path from the unauthenticated state to the authenticated state without providing correct credentials.

\begin{figure}[!ht]
  \includegraphics[width=0.5\textwidth]{imgs/hf-auth.pdf}
  \caption{The happy flow for the user authentication layer.}
  \label{fig:sshcomponents}
\end{figure}

\subsubsection{Connection protocol}\label{ssh-run-conn} 
The connection protocol's requests involve opening and closing channels, requesting a process over that channel and sending data to a requested process. Because the connection protocol offers a wide range of functionalities, it is hard to define a single happy flow. Requesting a terminal is one of the main features of SSH and has therefore been selected as the happy flow. This behaviour is typically triggered by the trace \textsc{ch\_open}; \textsc{ch\_request\_pty}. Its hard to define which behaviour would result in a state machine security flaw in this layer. We will therefore take a more general approach and look at unexpected state machine transitions that can point towards potential implementation flaws.

\begin{figure}[!ht]
  \includegraphics[width=0.5\textwidth]{imgs/hf-conn.pdf}
  \caption{The happy flow for the connection layer.}
  \label{fig:sshcomponents}
\end{figure}

